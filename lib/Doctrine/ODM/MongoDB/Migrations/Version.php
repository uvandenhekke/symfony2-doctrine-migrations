<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software consists of voluntary contributions made by many individuals
 * and is licensed under the LGPL. For more information, see
 * <http://www.doctrine-project.org>.
*/

namespace Doctrine\ODM\MongoDB\Migrations;

use Doctrine\ODM\MongoDB\Migrations\Configuration\Configuration;

/**
 * Class which wraps a migration version and allows execution of the
 * individual migration version up or down method.
 *
 * @license     http://www.opensource.org/licenses/lgpl-license.php LGPL
 * @link        www.doctrine-project.org
 * @since       2.0
 * @author      Jonathan H. Wage <jonwage@gmail.com>
 */
class Version
{
    const STATE_NONE = 0;
    const STATE_PRE  = 1;
    const STATE_EXEC = 2;
    const STATE_POST = 3;

    /**
     * The Migrations Configuration instance for this migration
     *
     * @var Configuration
     */
    private $configuration;

    /**
     * The OutputWriter object instance used for outputting information
     *
     * @var OutputWriter
     */
    private $outputWriter;

    /**
     * The version in timestamp format (YYYYMMDDHHMMSS)
     *
     * @param int
     */
    private $version;

    /**
     * The migration instance for this version
     *
     * @var AbstractMigration
     */
    private $migration;

    /**
     * @var \Doctrine\MongoDB\Connection
     */
    private $connection;
    
    /**
     * @var string
     */
    private $class;

    /** The array of collected SQL statements for this version */
    private $codes = array();

    /** The array of collected parameters for SQL statements for this version */
    private $args = array();

    /** The time in seconds that this migration version took to execute */
    private $time;

    /**
     * @var int
     */
    private $state = self::STATE_NONE;

    public function __construct(Configuration $configuration, $version, $class)
    {
        $this->configuration = $configuration;
        $this->outputWriter = $configuration->getOutputWriter();
        $this->class = $class;
        $this->connection = $configuration->getConnection();
        $this->migration = new $class($this);
        $this->version = $this->migration->getName() ?: $version;
    }

    /**
     * Returns the string version in the format YYYYMMDDHHMMSS
     *
     * @return string $version
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Returns the Migrations Configuration object instance
     *
     * @return Configuration $configuration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Check if this version has been migrated or not.
     *
     * @return boolean
     */
    public function isMigrated()
    {
        return $this->configuration->hasVersionMigrated($this);
    }

    public function markMigrated()
    {
        $this->configuration->insertMigratedVersion($this->version);
    }

    public function markNotMigrated()
    {
        $this->configuration->removeMigratedVersion($this->version);
    }

    /**
     * Add some JS queries to this versions migration
     *
     * @param array|string $code
     * @param array        $args
     *
     * @return void
     */
    public function addJs($code, array $args = array())
    {
        if (is_array($code)) {
            foreach ($code as $key => $query) {
                $this->codes[] = $query;
                if (isset($args[$key])) {
                    $this->args[count($this->codes) - 1] = $args[$key];
                }
            }
        } else {
            $this->codes[] = $code;
            if ($args) {
                $this->args[count($this->codes) - 1] = $args;
            }
        }
    }

    /**
     * Write a migration JS file to the given path
     *
     * @param string $path      The path to write the migration JS file.
     * @param string $direction The direction to execute.
     *
     * @return boolean $written
     */
    public function writeJsFile($path, $direction = 'up')
    {
        $queries = $this->execute($direction, true);

        $string  = sprintf("# Doctrine Migration ODM File Generated on %s\n", date('Y-m-d H:m:s'));

        $string .= "\n# Version " . $this->version . "\n";
        foreach ($queries as $query) {
            $string .= $query . ";\n";
        }
        if (is_dir($path)) {
            $path = realpath($path);
            $path = $path . '/doctrine_migration_' . date('YmdHis') . '.js';
        }

        $this->outputWriter->write("\n".sprintf('Writing migration file to "<info>%s</info>"', $path));

        return file_put_contents($path, $string);
    }

    /**
     * @return AbstractMigration
     */
    public function getMigration()
    {
        return $this->migration;
    }

    /**
     * Execute this migration version up or down and and return the JS.
     *
     * @param string  $direction The direction to execute the migration.
     * @param boolean $dryRun    Whether to not actually execute the migration JS and just do a dry run.
     *
     * @return array $js
     *
     * @throws \Exception when migration fails
     */
    public function execute($direction, $dryRun = false)
    {
        $this->js = array();

        try {
            $start = microtime(true);

            $this->state = self::STATE_PRE;
            $this->migration->{'pre' . ucfirst($direction)}($this->getConfiguration()->getDatabase());

            if ($direction === 'up') {
                $this->outputWriter->write("\n" . sprintf('  <info>++</info> migrating <comment>%s</comment>', $this->version) . "\n");
            } else {
                $this->outputWriter->write("\n" . sprintf('  <info>--</info> reverting <comment>%s</comment>', $this->version) . "\n");
            }

            $this->state = self::STATE_EXEC;

            $this->migration->$direction($this->getConfiguration()->getDatabase());

            if ($dryRun === false) {
                if ($this->codes) {
                    foreach ($this->codes as $key => $query) {
                        if ( ! isset($this->args[$key])) {
                            $this->outputWriter->write('     <comment>-></comment> ' . $query);
                            $response = $this->getConfiguration()->getDatabase()->execute($query);
                        } else {
                            $this->outputWriter->write(sprintf('    <comment>-</comment> %s (with parameters)', $query));
                            $response = $this->getConfiguration()->getDatabase()->execute($query, $this->args[$key]);
                        }
                        if ($response['ok'] == false) {
                            throw new \Exception("Can't execute the migration of query \"" . $query . "\" : " . var_export($response, true));
                        }
                    }
                } else {
                    $this->outputWriter->write(sprintf('<error>Migration %s was executed but did not result in any SQL statements.</error>', $this->version));
                }

                if ($direction === 'up') {
                    $this->markMigrated();
                } else {
                    $this->markNotMigrated();
                }

            } else {
                foreach ($this->codes as $query) {
                    $this->outputWriter->write('     <comment>-></comment> ' . $query);
                }
            }

            $this->state = self::STATE_POST;
            $this->migration->{'post' . ucfirst($direction)}($this->getConfiguration()->getDatabase());

            $end = microtime(true);
            $this->time = round($end - $start, 2);
            if ($direction === 'up') {
                $this->outputWriter->write(sprintf("\n  <info>++</info> migrated (%ss)", $this->time));
            } else {
                $this->outputWriter->write(sprintf("\n  <info>--</info> reverted (%ss)", $this->time));
            }

            $this->state = self::STATE_NONE;

            return $this->codes;
        } catch (SkipMigrationException $e) {
            if ($dryRun == false) {
                // now mark it as migrated
                if ($direction === 'up') {
                    $this->markMigrated();
                } else {
                    $this->markNotMigrated();
                }
            }

            $this->outputWriter->write(sprintf("\n  <info>SS</info> skipped (Reason: %s)",  $e->getMessage()));

            $this->state = self::STATE_NONE;

            return array();
        } catch (\Exception $e) {

            $this->outputWriter->write(sprintf(
                '<error>Migration %s failed during %s. Error %s</error>',
                $this->version, $this->getExecutionState(), $e->getMessage()
            ));

            $this->state = self::STATE_NONE;
            throw $e;
        }
    }

    public function getExecutionState()
    {
        switch ($this->state) {
            case self::STATE_PRE:
                return 'Pre-Checks';
            case self::STATE_POST:
                return 'Post-Checks';
            case self::STATE_EXEC:
                return 'Execution';
            default:
                return 'No State';
        }
    }

    /**
     * Returns the time this migration version took to execute
     *
     * @return integer $time The time this migration version took to execute
     */
    public function getTime()
    {
        return $this->time;
    }

    public function __toString()
    {
        return $this->version;
    }
}
